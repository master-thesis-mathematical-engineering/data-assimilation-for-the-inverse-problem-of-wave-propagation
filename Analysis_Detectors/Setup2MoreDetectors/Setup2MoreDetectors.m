%% Experiment section 6.2.3: Setup 2 is tested with 9 detectors
addpath('C:\Users\Maarten\Documents\Master_WIT\Thesis\Gitlab\MasterThesis')


clear all; close all; clc;
rng('default')

%% Problem Specifications
% Time- and space-interval
WavEqSpecs.T = 2;
WavEqSpecs.xmin = [-1,-1];
WavEqSpecs.xmax = [2,2];

% Coefficients
WavEqSpecs.ax = @(x,y) (1+x.*0+0.*y);
WavEqSpecs.ay = @(x,y) (1+x.*0+0.*y);

% Inclusion
WavEqSpecs.Cfun = @(x,y,param) (...
    (((x-param(1))/param(3)).^2+((y-param(2))/param(3)).^2-1));

% U on the inclusion
WavEqSpecs.f2 = @(x,y) (0+0.*x+0.*y);
WavEqSpecs.TypeIncl = @(x,y) ("Dirichlet");


%% Specifications Data Assimilation Setup
% Locations detectors
DataAssimSpecs.timetraces = [0.9,0.9;...
    0.9,0.7;...
    0.7,0.9;...
    0.9,0.5;...
    0.5,0.9;...
    0.9,0.3;...
    0.3,0.9;...
    0.1,0.9;...
    0.9,0.1];
Tstart = [170,140,140,110,110,80,80,50,50];


% Specifications Wave
DataAssimSpecs.originWave = [0.01,0];
DataAssimSpecs.Angle = pi/4;

%Area to be searched
xmin = [0.2,0.2,0.04]; 
xmax = [0.8,0.8,0.14];


%% Precision
dx = 0.01;


%% Data generation
noiselevel = 10^(-2);
trueParam = [0.7,0.4,0.1];
D = GenerateMeasurements(WavEqSpecs,DataAssimSpecs,trueParam,dx,noiselevel);

% Initialisation beta
varianceProposal =2.5*10^(-4);

% Initialisation prior, likelihood, proposal and forward solver
prior = @(param) prior(param,xmin,xmax);
likelihood = @(TTold,TTnew) likelihood(TTold,TTnew,D,Tstart,noiselevel);
proposal =  @(u) proposal(u,1,varianceProposal);
SolveFWProblem =  @(param) SolveFWProblem(WavEqSpecs,DataAssimSpecs,dx,param);


%% Experiment
 [Samples,AcceptanceRate,variance,variance2]= MarkovChainMonteCarlo(prior, likelihood,proposal,SolveFWProblem, ...
    10^(-6),3,'BurnIn',1000,'Transitionphase',...
    300,'varianceType',1,'nbOfRuns',10000);