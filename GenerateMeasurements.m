function D = GenerateMeasurements(WavEqSpecs,...
    DataAssimSpecs,trueParam,dx,noiselevel)
% D = GENERATEMEASUREMENTS(WAVEQSPECS,DATAASSIMSPECS,TRUEPARAM,DX,
% NOISELEVEL) A function that generates fictional measurements by
% solving the forward problem and adding random measurement noise. The
% function expects objects that specify the problem setting (WAVEQSPECS) and
% the data assimiltion context DATAASSIMSPECS. The true parameter values
% TRUEPARAM is required, just like the precision DX with which the
% simulation should be done and the amount of noise NPOSEMEVEM that should
% be introduced. The artificial data are returned as output

    % Solve forward problem
    TT = SolveFWProblem(WavEqSpecs,DataAssimSpecs,dx,trueParam);

    % Generate noise
    TT_noise = randn([size(TT,1),size(TT,2)]);

    % Add noise to the solution of the forward problem
    D  = TT + noiselevel*TT_noise;
end