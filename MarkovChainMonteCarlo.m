function [Samples,AcceptanceRate,variance,variance2] = ...
    MarkovChainMonteCarlo(prior, likelihood,proposal,SolveFWProblem, ...
    StopCondition,nbOfParam,varargin)
% [SAMPLES, ACCEPTANCERATE, VARIANCE, VARIANCE2] =
% MARKOVCHAINMONTECARLO(PRIOR,LIKELIHOOD,PROPOSAL,SOLVEFWPROBLEM,
% STOPCONDITION, NBOFPARAM, VARARGIN] Function that generates samples of a
% LIKELIHOOD with a Markov chain Monte Carlo method for a given PRIOR and 
% likelihood. A blackbox that solves the forward problem (SOLVEFWPROBLEM)
% is required as input. A proposal density PROPOSAL is expected as input as
% well. Finally, the stopcondition STOPCONDITION and the number of 
% parameters NBOFPARAM per inclusion have to be specified. The function
% returns the SAMPLES and possibly the evolution of the acceptance rate 
% (ACCEPTANCERATE) and variances (VARIANCE, VARIANCE2). The function 
% generates samples by generating a trial move with the proposal function.
% The posterior of the trial move is compared with the posterior of the 
% previous sample to decide upon acceptance. For this purpose the value a 
% is calculated that is equal to the ratio of both posteriors. A random 
% uniform variable is generated and if this value is smaller than a, the 
% proposed sample is kept, otherwise this sample is thrown away and the 
% previous samples is repeated This process is repeated until the variance 
% goes under a certain stopcriterium or until a prespecified number of 
% samples is generated.


    % Handling of the varargin
    stringlist = cell(1,length(varargin)/2);
    for i = 1:length(varargin)/2
        stringlist{i} = varargin{1+(i-1)*2};
    end
    stringlist = cellstr(stringlist);

    
    % Specify whether the moving variance is calculated or if the variance
    % is caluclated over all samples after the burn-in
    if any(strcmp(stringlist,'varianceType'))
        index = find(strcmp(stringlist,'varianceType'));
        TypeVar = varargin{index*2};
    else
        TypeVar = 1;
    end
    
    % Number of runs that is specified
    if any(strcmp(stringlist,'nbOfRuns'))
        index = find(strcmp(stringlist,'nbOfRuns'));
        RunsSpecified = true;
        nbOfRuns = varargin{index*2};
    else
        RunsSpecified = false;
        nbOfRuns = 100000;
    end

    % Number of inclusions that is specified
    if any(strcmp(stringlist,'nbOfInclusions'))
        index = find(strcmp(stringlist,'nbOfInclusions'));
        nbOfInclusions = varargin{index*2};
    else
        nbOfInclusions = 1;
    end
    

    % Initialisation of the burn-in time
    if any(strcmp(stringlist,'BurnIn'))
        index = find(strcmp(stringlist,'BurnIn'));
        BurnIn = varargin{index*2};
    else
        BurnIn = 0;
    end
    
    % Number of samples over which the moving variance is calculated
    if any(strcmp(stringlist,'Transitionphase'))
        index = find(strcmp(stringlist,'Transitionphase'));
        Transitionphase = varargin{index*2};
    else
        Transitionphase = 0;
    end
    
    % Quantify number of parameters that is estimated
    ParamtoEst = nbOfInclusions*nbOfParam;
   

    % If the number of samples is known in advance, memory is allocated to
    % store the variances
    if RunsSpecified == true
        if TypeVar == 1 || TypeVar == 3
            variance = zeros(ParamtoEst,nbOfRuns-Transitionphase+1);
            variance2 = zeros(ParamtoEst,nbOfRuns-Transitionphase+1);
        elseif TypeVar == 2
            variance = zeros(ParamtoEst,nbOfRuns-Transitionphase-BurnIn+1);
            variance2 = zeros(ParamtoEst,nbOfRuns-Transitionphase-BurnIn+1);
        end
    end

    % If the number of samples is known in advance, memory is allocated to
    % store the samples
    if RunsSpecified == true
        Samples = zeros(ParamtoEst,nbOfRuns);
    end

    % Initialsing first sample
    [~,r] = prior(zeros(ParamtoEst,1));
    Samples(:,1) = r;
    TTold = SolveFWProblem(r);

    i = 2;
    NbOfAcceptedSamples = 1;
    VarianceInside = 1;

    %Start of the while loop
    while VarianceInside >= StopCondition      
        % Propose new sample
        rprop = proposal(r);

        % Accept reject
        % Calculate a value
        TTprop = SolveFWProblem(rprop);
        a = rho(rprop,TTold,TTprop);
        
        % Decide upon accceptance
        if a < 1
            u = rand;

            % reject if u is larger than a
            if u > a
                Samples(:,i) = Samples(:,i-1);

            % Accept if u is smaller than a
            else
                r = rprop;
                NbOfAcceptedSamples = NbOfAcceptedSamples+1;
                Samples(:,i) = r;
                TTold = TTprop;
            end

        % Reject if a is larger than 1
        else
            r = rprop;
            NbOfAcceptedSamples= NbOfAcceptedSamples+1;
            Samples(:,i) = r;
            TTold = TTprop;
        end
        
        
            
        % Type 1: moving variance
        if  i >=Transitionphase && TypeVar == 1
                
            % variance of the variables
            for j = 1:ParamtoEst
                variance(j,i-Transitionphase+1) = ...
                    var(Samples(j,i-Transitionphase+1:i));
            end
            % variance in the direction of the principal components
            variance2(:,i-Transitionphase+1) = ...
                 eigs(cov(Samples(:,i-Transitionphase+1:i)'),9);
            
        % Type 2: variance that calculates variance over all samples
        % after the burn-in has passed
        elseif TypeVar == 2 && i >=  BurnIn + Transitionphase
                
            % variance of the variables
            for j = 1:ParamtoEst
                variance(j,i-(BurnIn + Transitionphase)+1) = ...
                    var(Samples(j,BurnIn+1:i));
            end
            % variance along principal components
             variance2(:,i-(BurnIn + Transitionphase)+1) = ...
                    eigs(cov(Samples(:,BurnIn+1:i)'),ParamtoEst);
        end
        if i >=  BurnIn + Transitionphase
            VarianceInside = ...
                    eigs(cov(Samples(:,BurnIn+1:i)'),1);
        end
        % Update acceptance rate
        AcceptanceRate(i-1)= NbOfAcceptedSamples/i;
        i = i+1;

        % Stop loop if nbOfRuns is specified
        if RunsSpecified == true
            if i >= nbOfRuns
                break
            end
        end
    end
    nbOfSimulations = i;


    function a = rho(param,TTold,TTprop)
    % A = RHO(PARAM,TTOLD,TTPROP) function that calculates the a-value, the
    % ratio of the two posteriors
        
        % Return 0 if the proposed samples i outside the coverage of the
        % prior distirubtion
        if prior(param) == 0
            a = 0;
        % Calculate ratio's of likelihood
        else
            a = likelihood(TTold,TTprop);
        end
    end
end

