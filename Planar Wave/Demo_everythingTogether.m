clear all; close all; clc;
%% Script
% Planar Wave
% Inclusion: Circle, Dirichlet BC
% Variable coefficients:ax = @(x,y) (x.*y); ay = @(x,y) (exp(x.*y));
% nonHomogeneous BC


%% Specifications problem
% Time- and space-interval
T = 1;
xmin = [0,0];
xmax = [1,1];

% Timetraces
timetraces = [];

% Specify properties planar wave
origin = [0.01, 0.0];
angle = pi/4; % in radians
Tmatrix = [cos(angle), sin(angle); -sin(angle),cos(angle)]; % rotation in clockwise direction

% Offset 
Offset = 15;

% Coëfficiëntss
ax = @(x,y) (exp(-x.^2-y.^2));
ay = @(x,y) (exp(-x.^2-y.^2));

% InitialConditions
g = @(x,y) (Offset + exp(-250.*((Tmatrix(1,1).*(x-origin(1))+Tmatrix(1,2).*(y-origin(2))).^2)));
h = @(x,y) (exp(-250.*((Tmatrix(1,1).*(x-origin(1))+Tmatrix(1,2).*(y-origin(2))).^2))...
    .*sqrt(0.5*(ax(x,y)+ay(x,y)))*500.*(Tmatrix(1,1).*(x-origin(1))+Tmatrix(1,2).*(y-origin(2))));

% Boundary Conditions
f1 = @(x,y,t) (Offset+exp(-250.*((Tmatrix(1,1).*(x-origin(1))+Tmatrix(1,2).*(y-origin(2))-sqrt(0.5*(ax(x,y)+ay(x,y)))*t).^2)));


% Inclusion
C = @(x,y) (((x-0.5)/0.1).^2+((y-0.5)/0.1).^2-1);

% U on the inclusion
f2 = @(x,y) (Offset+0.*x+0.*y);
TypeIncl = @(x,y) ("Dirichlet");



%% Discretisation
% subsequent refinements
list = [0.03125,0.015625,0.015625/2,0.015625/4,0.015625/8,0.015625/16,0.015625/32];


%% Allocation memory
err = zeros(1,length(list)-1);


%% Execution of the different simulations
for i = 1:length(list)
        
    % Discretisation
    dx = list(i);
      
    
    % Apply finite differences
    U = Simulate(dx, T, xmin, xmax, ax,ay,g, h, f1,"inclusion",{C,f2,TypeIncl});

    
    % Mesh generaton
    x = xmin(1)+dx:dx:xmax(1)-dx;
    y = xmin(2)+dx:dx:xmax(2)-dx;
    [X,Y] = meshgrid(x);
    
    
    % Plot soltution
    figure(2)
    surf(X,Y,U)
    title('Solution')
    xlabel('x')
    ylabel('y')
    zlim([Offset-2, Offset+1])
        
    if i >= 2
        % Calculate size of error of previous solution
        err(i-1) = max(max(abs((Uprev - U(2:2:end,2:2:end)))));
        
        % Plot error
        figure(3)
        surf(X(2:2:end,2:2:end),Y(2:2:end,2:2:end),abs(Uprev - U(2:2:end,2:2:end)));
        title('Error in meshpoints')
        xlabel('x')
        ylabel('y')
    end 
    Uprev = U; 
end


%% Order plot
figure(4)
loglog(list(1:end-1),err,'--o','Color',[0.4940 0.1840 0.5560],'MarkerSize',8)
xlabel('dx')
legend('||U-U_{ref}||_{\infty}','reference line')