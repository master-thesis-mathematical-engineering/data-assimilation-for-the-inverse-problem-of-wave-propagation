clear all; close all; clc;
%% Script
% Planar Wave
% Inclusion: Ellips, Dirichlet BC
% No variable coefficients
% Homogeneous BC

%% Specifications problem
% Time- and space-interval
T = 1;
xmin = [0,0];
xmax = [1,1];

% Timetraces (None)
timetraces = [];

% Specify properties planar wave
origin = [0.01, 0];
angle = pi/4; % in radians
Tmatrix = [cos(angle), sin(angle); -sin(angle),cos(angle)]; % rotation in clockwise direction

% Offset
Offset = 0; 

% InitialConditions
g = @(x,y) (Offset + exp(-250.*((Tmatrix(1,1).*(x-origin(1))+Tmatrix(1,2).*(y-origin(2))).^2)));
h = @(x,y) (exp(-250.*((Tmatrix(1,1).*(x-origin(1))+Tmatrix(1,2).*(y-origin(2))).^2))...
    .*500.*(Tmatrix(1,1).*(x-origin(1))+Tmatrix(1,2).*(y-origin(2))));

% Boundary Conditions
f1 = @(x,y,t) (Offset+exp(-250.*((Tmatrix(1,1).*(x-origin(1))+Tmatrix(1,2).*(y-origin(2))-t).^2)));

% Coëfficiëntss
ax = @(x,y) (1+0.*x+0.*y);
ay = @(x,y) (1+0.*x+0.*y);

% Inclusion
C = @(x,y) ((((x-0.5)/0.08).^2+((y-0.5)/0.08).^2-1).*...
    (((x-0.8)/0.04).^2+((y-0.3)/0.04).^2-1));
%
% U on the inclusion
f2 = @(x,y) (Offset+0.*x+0.*y);
TypeIncl = @(x,y) ("Dirichlet");



%% Discretisation
% subsequent refinements
list = [0.03125,0.015625,0.015625/2,0.015625/4,0.015625/8,0.015625/16,0.015625/32];

%% Allocation memory
err = zeros(1,length(list)-1);

%% Execution of the different simulations
for i = 1:length(list)
    % Discretisation        
    dx = list(i);
         
    % Apply finite differences
    tic
    U = Simulate(dx, T, xmin, xmax, ax,ay,g, h, f1,"inclusion",{C,f2,TypeIncl});
    toc
    
    % Mesh generaton
    x = xmin(1)+dx:dx:xmax(1)-dx;
    y = xmin(2)+dx:dx:xmax(2)-dx;
    [X,Y] = meshgrid(x,y);

 
    
    % Plot soltution
    figure(2)
    surf(X,Y,U)
    title('Solution')
    xlabel('x')
    ylabel('y')
        
    if i >= 2
        % Calculate size of error of previous solution
        err(i-1) = max(max(abs((Uprev - U(2:2:end,2:2:end)))));
        
        % Plot error
        figure(3)
        surf(X(2:2:end,2:2:end),Y(2:2:end,2:2:end),abs(Uprev - U(2:2:end,2:2:end)));
        title('Error in meshpoints')
        xlabel('x')
        ylabel('y')
    end 
    Uprev = U;
end

figure(4)
loglog(list(1:end-1),err,'--o','Color', [0.9290 0.6940 0.1250],'MarkerSize',8)
xlabel('dx')
legend('||U-U_{ref}||_{\infty}','reference line')
