clear all; close all; clc;
% demo_PlanarWbasic
% saveas(4,'figure_PSBasic.fig')
% 
% clear all; close all;
% demo_PlanarWInclusion
% saveas(4,'figure_inclusion.fig')

% clear all; close all;
% demo_PlanarWvarCoeff2
% saveas(4,'figure_varCoeff.fig')
% 
% clear all; close all;
% demo_PlanarNonHomogeneousBC
% saveas(4,'figure_nonHomogeneous.fig')

% clear all; close all;
% Demo_multipleinclusions
% saveas(4,'figure_multipleinclusions.fig')
% 
% clear all; close all;
% Demo_everythingTogether
% saveas(4,'figure_everythingtogether.fig');



close all; clc; clear all;
figure(1)
list = [0.03125,0.015625,0.015625/2,0.015625/4,0.015625/8,0.015625/16];
loglog(list,1/(0.03125^2).*list.^2)
hold on

openfig('figure_PSBasic.fig')
L = findobj(2,'type','line');
copyobj(L,findobj(1,'type','axes'));
close(2)

openfig('figure_inclusion.fig')
L = findobj(2,'type','line');
copyobj(L,findobj(1,'type','axes'));
close(2)

openfig('figure_varCoeff.fig')
L = findobj(2,'type','line');
copyobj(L,findobj(1,'type','axes'));
close(2)

% openfig('figure_nonHomogeneous.fig')
% L = findobj(2,'type','line');
% copyobj(L,findobj(1,'type','axes'));
% close(2)


openfig('figure_multipleinclusions.fig')
L = findobj(2,'type','line');
copyobj(L,findobj(1,'type','axes'));
close(2)

openfig('figure_everythingtogether.fig')
L = findobj(2,'type','line');
copyobj(L,findobj(1,'type','axes'));
close(2)

% openfig('referenceline.fig')
% L = findobj(2,'type','line');
% copyobj(L,findobj(1,'type','axes'));
% close(2)

legend('C\Deltax','Rectangular domain','Domain with inclusion', ...
    'Variable coefficients','Multiple Inclusions', ...
    'Inclusion and variable coefficients', ...
    'Location','northwest')
xlabel('$\Delta$ x')
ylabel('Discretisation Error')
