clear all; close all; clc;

rng('default')

%% Specifications problem
% Time- and space-interval
WavEqSpecs.T = 1.42; %Angle of 45 degrees
%WavEqSpecs.T = 0.895;

WavEqSpecs.xmin = [0,0];
WavEqSpecs.xmax = [1,1];

% Boundary Conditions
WavEqSpecs.f1 = @(x,y,t) (exp(-250*(((x-t)-0.25).^2)));

% Coefficients
WavEqSpecs.ax = @(x,y) (1+x.*0+0.*y);
WavEqSpecs.ay = @(x,y) (1+x.*0+0.*y);

% Inclusion
WavEqSpecs.param =[0.3,0.5,0.07];
WavEqSpecs.Cfun = @(x,y,param) (((x-param(1))/param(3)).^2+((y-param(2))/param(3)).^2-1);

% U on the inclusion
WavEqSpecs.f2 = @(x,y) (0+0.*x+0.*y);
WavEqSpecs.TypeIncl = @(x,y) ("Dirichlet");

%% Specifications Data Assimilation
% Locations time traces set 1
DataAssimSpecs.timetraces = [0.9,0.9];


% Specifications Wave
DataAssimSpecs.originWave = [0.1,0];
DataAssimSpecs.Angle = pi/4; % 0 -> parallel to the y-as, 
                          % moves in the direction of the x-as

[error1,error2] = ErrorByBC(WavEqSpecs,DataAssimSpecs,0.01);

%Area to be search
xmin = [0.2,0.2,0]; 
xmax = [0.7,0.7,0.1];

figure(1)
plot(0.9:-0.01:0.51,error2)
xlabel('Center Inclusion')
ylabel('Absolute error')

figure(2)
plot(0.9:-0.01:0.51,error1)
xlabel('Center Inclusion')
ylabel('Relative error')


