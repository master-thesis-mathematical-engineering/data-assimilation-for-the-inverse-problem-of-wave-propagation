clear all; close all; clc;

rng('default')

%% Specifications problem
% Time- and space-interval
WavEqSpecs.T = 1.5; %Angle of 45 degrees
%WavEqSpecs.T = 0.895;

WavEqSpecs.xmin = [0,0];
WavEqSpecs.xmax = [1,1];

% Boundary Conditions
WavEqSpecs.f1 = @(x,y,t) (exp(-250*(((x-t)-0.25).^2)));

% Coefficients
WavEqSpecs.ax = @(x,y) (1+x.*0+0.*y);
WavEqSpecs.ay = @(x,y) (1+x.*0+0.*y);

% Inclusion
WavEqSpecs.param =[0.3,0.7,0.07];
WavEqSpecs.Cfun = @(x,y,param) ( ...
    (((x-param(1))/param(3)).^2+((y-param(2))/param(3)).^2-1)...
    );

% U on the inclusion
WavEqSpecs.f2 = @(x,y) (0+0.*x+0.*y);
WavEqSpecs.TypeIncl = @(x,y) ("Dirichlet");

%% Specifications Data Assimilation
% Locations time traces
DataAssimSpecs.timetraces = [0.1,0.1; 0.1,0.5;...
    0.5,0.1];


% Specifications Wave
DataAssimSpecs.originWave = [0.1,0];
DataAssimSpecs.Angle = pi/4; % 0 -> parallel to the y-as, 
                          % moves in the direction of the x-as

%% Discretisation
dx = 0.01;

TT = SolveFWProblem(WavEqSpecs,DataAssimSpecs,dx);                          



