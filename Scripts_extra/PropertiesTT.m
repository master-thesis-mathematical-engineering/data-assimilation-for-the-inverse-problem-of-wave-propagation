clear all; close all; clc;

%%
rng('default')

%% Specifications problem
% Time- and space-interval
WavEqSpecs.T = 2.5; %Angle of 45 degrees %220
%WavEqSpecs.T = 0.895;

WavEqSpecs.xmin = [-1,-1];
WavEqSpecs.xmax = [2,2];

% Coefficients
WavEqSpecs.ax = @(x,y) (1+x.*0+0.*y);
WavEqSpecs.ay = @(x,y) (1+x.*0+0.*y);

% Inclusion
WavEqSpecs.param =[0.4,0.8,0.1];
WavEqSpecs.Cfun = @(x,y,param) (...
    (((x-param(1))/param(3)).^2+((y-param(2))/param(3)).^2-1));


% U on the inclusion
WavEqSpecs.f2 = @(x,y) (0+0.*x+0.*y);
WavEqSpecs.TypeIncl = @(x,y) ("Dirichlet");

%% Specifications Data Assimilation
% Locations time traces
% DataAssimSpecs.timetraces = ...
%     [0.1,0.5; ...
%     0.5,0.1;...
%     0.9,0.5;...
%     0.5,0.9];

DataAssimSpecs.timetraces= [0.1,0.5;0.1,0.7;0.7,0.1;0.3,0.1;0.1,0.3;0.5,0.1;0.1,0.1];

% Specifications Wave
DataAssimSpecs.originWave = [0.1,0];
DataAssimSpecs.Angle = pi/4; % 0 -> parallel to the y-as, 
                          % moves in the direction of the x-as

%% Discretisation
dx = 0.01;

TT1 = SolveFWProblem(WavEqSpecs,DataAssimSpecs,dx);    

% Time- and space-interval
WavEqSpecs.T = 2.5; %Angle of 45 degrees %220
%WavEqSpecs.T = 0.895

WavEqSpecs.xmin = [0,0];
WavEqSpecs.xmax = [1,1];

% Coefficients
WavEqSpecs.ax = @(x,y) (1+x.*0+0.*y);
WavEqSpecs.ay = @(x,y) (1+x.*0+0.*y);

% Inclusion
WavEqSpecs.param =[0.4,0.8,0.1];
WavEqSpecs.Cfun = @(x,y,param) (...
    (((x-param(1))/param(3)).^2+((y-param(2))/param(3)).^2-1));


% U on the inclusion
WavEqSpecs.f2 = @(x,y) (0+0.*x+0.*y);
WavEqSpecs.TypeIncl = @(x,y) ("Dirichlet");

%% Specifications Data Assimilation
% Locations time traces
% DataAssimSpecs.timetraces = ...
%     [0.1,0.5; ...
%     0.5,0.1;...
%     0.9,0.5;...
%     0.5,0.9];

DataAssimSpecs.timetraces= [0.1,0.5;0.1,0.7;0.7,0.1;0.3,0.1;0.1,0.3;0.5,0.1;0.1,0.1];

% Specifications Wave
DataAssimSpecs.originWave = [0.1,0];
DataAssimSpecs.Angle = pi/4; % 0 -> parallel to the y-as, 
                          % moves in the direction of the x-as

%% Discretisation
dx = 0.01;

TT2 = SolveFWProblem(WavEqSpecs,DataAssimSpecs,dx);    

figure(1)
hold on
plot(0:0.005:2.5,TT1(1,1:501),'b')
plot(0:0.005:2.5,TT2(1,1:501),'r--')
legend('Buffer zone','No buffer zone')
xlabel('t')
ylabel('Wave intensity')

figure(2)
hold on
plot(0:0.005:2.5,TT1(2,1:501),'b')
plot(0:0.005:2.5,TT2(2,1:501),'r--')
legend('Buffer zone','No buffer zone')
xlabel('t')
ylabel('Wave intensity')

figure(3)
hold on
plot(0:0.005:2.5,TT1(3,1:501),'b')
plot(0:0.005:2.5,TT2(3,1:501),'r--')
legend('Buffer zone','No buffer zone')
xlabel('t')
ylabel('Wave intensity')

figure(4)
hold on
plot(0:0.005:2.5,TT1(4,1:501),'b')
plot(0:0.005:2.5,TT2(4,1:501),'r--')
legend('Buffer zone','No buffer zone')
xlabel('t')
ylabel('Wave intensity')

figure(5)
hold on
plot(0:0.005:2.5,TT1(5,1:501),'b')
plot(0:0.005:2.5,TT2(5,1:501),'r--')
legend('Buffer zone','No buffer zone')
xlabel('t')
ylabel('Wave intensity')

figure(6)
hold on
plot(0:0.005:2.5,TT1(6,1:501),'b')
plot(0:0.005:2.5,TT2(6,1:501),'r--')
legend('Buffer zone','No buffer zone')
xlabel('t')
ylabel('Wave intensity')

figure(7)
hold on
plot(0:0.005:2.5,TT1(7,1:501),'b')
plot(0:0.005:2.5,TT2(7,1:501),'r--')
legend('Buffer zone','No buffer zone')
xlabel('t')
ylabel('Wave intensity')
