clear all; close all; clc;

%%
rng('default')

%% Specifications problem
% Time- and space-interval
WavEqSpecs.T = 2.5; %Angle of 45 degrees %220
%WavEqSpecs.T = 0.895;

WavEqSpecs.xmin = [-1,-1];
WavEqSpecs.xmax = [2,2];

% Coefficients
WavEqSpecs.ax = @(x,y) (1+x.*0+0.*y);
WavEqSpecs.ay = @(x,y) (1+x.*0+0.*y);

% Inclusion
WavEqSpecs.param =[0.2,0.8,0.1];
WavEqSpecs.Cfun = @(x,y,param) (...
    (((x-param(1))/param(3)).^2+((y-param(2))/param(3)).^2-1));


% U on the inclusion
WavEqSpecs.f2 = @(x,y) (0+0.*x+0.*y);
WavEqSpecs.TypeIncl = @(x,y) ("Dirichlet");

%% Specifications Data Assimilation
% Locations time traces
% DataAssimSpecs.timetraces = ...
%     [0.1,0.5; ...
%     0.5,0.1;...
%     0.9,0.5;...
%     0.5,0.9];

DataAssimSpecs.timetraces= [0.9,0.3];

% Specifications Wave
DataAssimSpecs.originWave = [0.01,0];
DataAssimSpecs.Angle = pi/4; % 0 -> parallel to the y-as, 
                          % moves in the direction of the x-as

%% Discretisation
dx = 0.01;

TT = SolveFWProblem(WavEqSpecs,DataAssimSpecs,dx);     

figure(1)
hold on
plot(1:502,TT(1,:),'b')
plot(1:502,TT(2,:),'r--')
plot(1:502,TT(3,:),'yo')


