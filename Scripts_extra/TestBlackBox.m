clear all; close all; clc;


%% Specifications problem
% Time- and space-interval
WavEqSpecs.T = 0.7;
WavEqSpecs.xmin = [0,0];
WavEqSpecs.xmax = [1,1];


%Locations time traces
DataAssimSpecs.timetraces = [0.1,0.2;0.7,0.8];
DataAssimSpecs.originWave = [0.25,0.5];
DataAssimSpecs.Angle = 0;

% Boundary Conditions
WavEqSpecs.f1 = @(x,y,t) (exp(-250*(((x-t)-0.25).^2)));

% Coëfficiëntss
WavEqSpecs.ax = @(x,y) (1+x.*0+0.*y);
WavEqSpecs.ay = @(x,y) (1+x.*0+0.*y);

% Inclusion
WavEqSpecs.c = [0.5,0.5];
WavEqSpecs.Cfun = @(x,y,c) (((x-c(1))/0.1).^2+((y-c(2))/0.1).^2-1);

% U on the inclusion
WavEqSpecs.f2 = @(x,y) (0+0.*x+0.*y);
WavEqSpecs.TypeIncl = @(x,y) ("Dirichlet");


% Discretisation used
dx = 0.01;

% Simulation
TT = SolveFWProblem(WavEqSpecs,DataAssimSpecs,dx);
