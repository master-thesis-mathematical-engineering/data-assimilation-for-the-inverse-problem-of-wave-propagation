function [U,TT] = Simulate(dx, T, xmin, xmax, axfun,ayfun, ...
    g, h, f1,varargin)
% [U,TT] = SIMULATE(DX,T,XMIN,XMAX,AXFUN,AYFUN,G,H,F1,VARARGIN). This
% funciton applies the theory developed in this thesis to solve the wave
% equation. A finite differences scheme is used for the spatial
% discretisation. The time discretisation combines an implicit unconditionally
% stable scheme near the inclusions with an explicit scheme farther away.
% The functions expects the full specification of the wave eqution i.e. the
% functions G, H, F1 and in case of an inclusion F2 and C. The spatial domain is
% specified with the vectors XMIN and XMAX, the time interval is given by
% its endpoint T. Additional VARARGIN are possible to request an evolution
% plot or to request the function to return time traces in which case their
% locations need to be specified.
  

    % Handling of the varargin  
    stringlist = cell(1,length(varargin)/2);
    for i = 1:length(varargin)/2
        stringlist{i} = varargin{1+(i-1)*2};
    end
    stringlist = cellstr(stringlist);

    % If an inclusion is specified, the functions C, f2 are entered
    if any(strcmp(stringlist,'inclusion'))
        index = find(strcmp(stringlist,'inclusion'));
        inclusion = true;
        cellInclusion = varargin{index*2};
        C = cellInclusion{1};
        f2 = cellInclusion{2};
        TypeIncl = cellInclusion{3};
    else
        inclusion = false;
    end
   
    % Boolean to request an evolution plot
    if any(strcmp(stringlist,'evolution'))
        index = find(strcmp(stringlist,'evolution'));
        evolution =varargin{index*2};
    else
        evolution = false;
    end
    

    % Boolean that specifies the location where time traces should be
    % returned
    if any(strcmp(stringlist,'timetraces'))
        index = find(strcmp(stringlist,'timetraces'));
        timetraces = varargin{index*2};
        TTboolean = true;
    else
        TTboolean = false;
    end
    
    
    % Numbe of meshpoints in both direction
    Nx = (xmax(1)-xmin(1))/dx-1;
    Ny = (xmax(2)-xmin(2))/dx-1;

    % Distinguish whether f1 is dependent on time or not
    if nargin(f1)>length(xmin)
        tempdepbc = true;
    else
        tempdepbc = false;
    end
    
     TT = 0;  
    

    % Evaluate functions in meshpoints 
    % ax(x,y), ay(x,y) on auxiliry grid
    [ax, ay] = coefficientsbetweennodes(axfun,ayfun);    
    % f(x,y) & g(x,y) on main grid
    [U0,dUdt0] = evaluatefinmesh(g,h);
    

    % Space discretisation over rectangular domain
    A = SpatialDiscretisationRectDomain(ax,ay, Nx, Ny);
    b1 = CalculateB(f1,ax,ay,dx,xmin,xmax,0);

    % Adapt discretisation in case of an inclusion
    if inclusion == true
        [A1,A2, b2,outsideidxs] = ...
            SpatialDiscretisationForInclusion(A,dx,C,xmin,xmax,axfun,ayfun,...
            f2,TypeIncl,f1);
    end


    % Determine size of the timestep: time step restriction is taken into
    % account
    if any(strcmp(stringlist,"dt"))
        index = find(strcmp(stringlist,"dt"));
        dt =  varargin{index*2};
    else
        max1 = max(ax); max2 = max(ay);
        dt = dx*0.5/max(max1,max2); 
    end
    
    % Initialise the first two values for U
    if inclusion
        U = initialisationU(A1+A2,b1+b2,U0,dUdt0,outsideidxs);
    else
        U = initialisationU(A,b1,U0,dUdt0,[]);
    end


    % Initialise matrices needed for the time stepping procedure
    if inclusion
        Cm = invertMatrix(A1);
        Am = Cm*A2*(dt^2)/(dx^2)+2*Cm;
        Bm = Cm*((dt^2)/(2*dx^2)*A1-speye(size(A1)));   
    else
        Am = (dt^2)/(dx^2)*A + 2*speye(size(A));
        Bm = -speye(size(A));
        Cm = speye(size(A));
    end

    


    

    
    % Time stepping procedure
    dtold = dt;
    if TTboolean
        % Set-up of the time tracing
        [Idx, Fracs] = LocalisationTT;
        TT = initialisationTT(U,Idx,Fracs);
        % Execute time stepping, register U at the end and timetraces
        if inclusion
            [U,TT] = timeStepping(U,Am,Bm,Cm,b1,'b2',b2, ...
            'TT',{TT,Idx, Fracs});
        else
            [U,TT] = timeStepping(U,Am,Bm,Cm,b1,'TT',{TT,Idx, Fracs});
        end
    else
        % Execute time stepping, register U at the end
        if inclusion
            U = timeStepping(U,Am,Bm,Cm,b1,'b2', b2);
        else
            U = timeStepping(U,Am,Bm,Cm,b1);
        end

    end
    

    % Make one additional time step, when the amount of time steps needed
    % is not an integer number in which case a smaller time step needs to
    % be set. dt will be equal to this value. 
    % 
    % To see whether this value is needed, dt is compared with the original
    % value
    if dt < dtold && dt ~= 0

        % new time step is defined to keep the second order
        k = dt/dtold;
        dtsqnew = 0.5*(1+k)*dtold^2;

        % 1 time step is executed
        if tempdepbc == true
            b1 = CalculateB(f1,ax,ay,dx,xmin,xmax,T-dt);
        end
        if inclusion
            bnew=k*(dtsqnew)/(dx^2)*(b1+b2);
            Cm = invertMatrixSmallerTS(A1,dtsqnew,k);
            Am = Cm*k*A2*(dtsqnew)/(dx^2)+(k+1)*Cm;
            Bm = Cm*(k*(dtsqnew)/(2*dx^2)*A1-k*speye(size(A1)));   
        else
            bnew=k*(dtsqnew)/(dx^2)*b1;
            Am = k*(dtsqnew)/(dx^2)*A + (k+1)*speye(size(A));
            Bm = -k*speye(size(A));
            Cm = speye(size(A));
        end
        Unew = Am*U(:,1)+Bm*U(:,2) +Cm*bnew;                   

        % Update values
        U(:,2) = U(:,1); U(:,1) = Unew;
    end
    

    % The output is returned
    U = U(:,1);
    U = reshape(U,[Nx,Ny]);
    U = U';


  
   


    function [fdiscr1,fdiscr2] = evaluatefinmesh(f1,f2)
        % [fdiscr1,fdiscr2] = EVALUATEFINMESH(F1,F2) This function
        % calculates the values of the functions F1 and F2 at the mesh
        % points of the grid. EVALIATEFINMESH expects two function handles 
        % with tho variables as inputs and returns two real arrays 
        % as outputs.
        
        % Check whether two functions are function handles
        if ~isa(f1,'function_handle') 
            error('ERROR IN EVALUATEFINMESH: f1 must be a function handle')
        elseif nargin(f1) ~= 2
            error('ERROR IN EVALUATEFINMESH: f1 must have two variables')            
        elseif ~isa(f2,'function_handle') 
            error('ERROR IN EVALUATEFINMESH: f2 must be a function handle')
        elseif nargin(f2) ~= 2
            error('ERROR IN EVALUATEFINMESH: f2 must have two variables')
        end            

        % initialisation of the grid
        x = xmin(1)+dx:dx:xmax(1)-dx;
        y = xmin(2)+dx:dx:xmax(2)-dx;
        [Y,X] = meshgrid(y,x);
        X = reshape(X,[Nx*Ny,1]);
        Y = reshape(Y,[Nx*Ny,1]);        

        % evaluation of the function at the gridpoits
        fdiscr1 = f1(X,Y); 
        fdiscr2 = f2(X,Y);        
        
    end

    function [ax, ay] = coefficientsbetweennodes(axfun,ayfun)
    % [AX, AY] = COEFFICIENTSBETWEENNODES(AXFUN,AYFUN) This function
    % calculates the values of the wave speed at the mesh points of the
    % auxiliary grids:
    % - AX = [ax(1/2*dx,dy), ax(3/2*dx,dy), ax(5/2*dx,dy), ...,
    % ax(1/2*dx,2*dy),..., ax((Nx-1/2)*dx,Ny*dy)]
    % - AY = [ax(dx,1/2*dy), ax(dx,3/2*dy), ax(dx,5/2*dy), ...,
    % ax(2*dx,1/2*dy),..., ax(Nx*dx,(Ny-1/2)*dy)]
    % COEFFICIENTSBETWEENNODES expects two function handles axfun and ayfun
    % with two arguments as inputs. It returns two real valued arrays as 
    % outputs
        
        % Check whether two functions are function handles
        if ~isa(axfun,'function_handle') 
            error(['ERROR IN COEFFICIENTSBETWEENNODES: axfun must be a ' ...
                'function handle'])
        elseif nargin(axfun) ~= 2
            error(['ERROR IN COEFFICIENTSBETWEENNODES: axfun must have ' ...
                'two input arguments'])            
        elseif ~isa(axfun,'function_handle') 
            error(['ERROR IN COEFFICIENTSBETWEENNODES: ayfun must be a ' ...
                'function handle'])
        elseif nargin(ayfun) ~= 2
            error(['ERROR IN COEFFICIENTSBETWEENNODES: ayfun must have ' ...
                'two input arguments'])          
        end      


        % Initialisation of the auxiliary grid for ax
        x = xmin(1)+dx/2:dx:xmax(1)-dx/2;
        y = xmin(2)+dx:dx:xmax(2)-dx;
        [Y,X] = meshgrid(y,x);
        X = reshape(X,[length(x)*length(y),1]);
        Y = reshape(Y,[length(x)*length(y),1]);
        
        % Evaluation of axfun on the gridpoints
        ax = axfun(X,Y);
  
        % Initialisation of the auxiliary grid for ay
        x = xmin(1)+dx:dx:xmax(1)-dx;
        y = xmin(2)+dx/2:dx:xmax(2)-dx/2;
        [Y,X] = meshgrid(y,x);
        X = reshape(X,[length(x)*length(y),1]);
        Y = reshape(Y,[length(x)*length(y),1]);
        
        % Evaluation of ayfun on the gridpoints
        ay = ayfun(X,Y);
    end

    function C = invertMatrix(A1)
    % C = INVERTMATRIX(A1,DT,D) This function caldculates the inverse of
    % the diagonal matrix I + (dt^2/2*dx^2)*A1. Since this matrix is
    % diagonal, it suffices to invert all diagonal elements. INVERTMATRIX
    % expects a real matrix A1 of dimension Nx*Ny-by-Nx*Ny as input. 
    % The function returns a real valued matrix C.
        
        % check whether A1 has the right size and is of the right type
        if size(A1,1)~= Nx*Ny || size(A1,2)~= Nx*Ny
            error(['ERROR IN INVERTMATRIX: size A1 ' ...
             'is incompatible with size grid'])
        elseif ~isreal(A1)
            error('ERROR IN INVERTMATRIX: A1 must be a real matrix')
        end
        
        % Creation of array with the inverted diagonal elements
        elem = 1./(1-(dt^2)/(2*dx^2).*diag(A1));

        % Initialisation of the sparse inverse matrix
        C = sparse(1:size(A1),1:size(A1),elem);
    end

    function C = invertMatrixSmallerTS(A1,dtnewsq,k)
    % C = invertMatrixSmallerTS(A1,DT,D) This function caldculates the inverse of
    % the diagonal for the last small timestep
        
        % check whether A1 has the right size and is of the right type
        if size(A1,1)~= Nx*Ny || size(A1,2)~= Nx*Ny
            error(['ERROR IN INVERTMATRIX: size A1 ' ...
             'is incompatible with size grid'])
        elseif ~isreal(A1)
            error('ERROR IN INVERTMATRIX: A1 must be a real matrix')
        end
        
        % Creation of array with the inverted diagonal elements
        elem = 1./(1-k*(dtnewsq)/(2*dx^2).*diag(A1));

        % Initialisation of the sparse inverse matrix
        C = sparse(1:size(A1),1:size(A1),elem);
    end


    function U = initialisationU(A,b,U0,dUdt0,outsideidxs)
    % U = INITIALISATIONU(A,B,U0,DUDT0) This function determines the first
    % two values for the vector of discrete values U i.e. U for t=0 and 
    % t=-dt. This is done through the following two techniques:
    %  - t = 0: U = U0
    %  - t = -dt: Taylor expansion for the time of second order + second
    %  order approximation for the second derivative to time
    %    U(.,-dt) = U(.,0)-dt*delta U/delta t+(dt^2/2)*delta^2 U/delta t^2
    %    + O(dt^3)
        
        % check whether A has the right size and is of the right type
        if size(A,1)~= Nx*Ny || size(A,2)~= Nx*Ny
            error(['ERROR IN INITIALISATIONU: size A ' ...
             'is incompatible with size grid'])
        elseif ~isreal(A)
            error('ERROR IN INITIALISATIONU: A must be a real matrix')
        end

        % check whether b has the right size and is of the right type
        if length(b)~= Nx*Ny || size(b,2)~= 1
            error(['ERROR IN INITIALISATIONU: size b ' ...
             'is incompatible with size grid'])
        elseif ~isreal(b)
            error('ERROR IN INITIALISATIONU: b must be a real matrix')
        end

        % check whether U0 has the right size and is of the right type
        if length(U0)~= Nx*Ny || size(U0,2)~= 1
            error(['ERROR IN INITIALISATIONU: size U0 ' ...
             'is incompatible with size grid'])
        elseif ~isreal(U0)
            error('ERROR IN INITIALISATIONU: U0 must be a real matrix')
        end

        % check whether dUdt0 has the right size and is of the right type
        if length(dUdt0)~= Nx*Ny || size(dUdt0,2)~= 1
            error(['ERROR IN INITIALISATIONU: size dUdt0 ' ...
             'is incompatible with size grid'])
        elseif ~isreal(dUdt0)
            error('ERROR IN INITIALISATIONU: dUdt0 must be a real matrix')
        end

        
              
        % Memory allocation
        U = zeros(Nx*Ny,2);

        % Second order scheme to determine U at t = 0 and U at t = -dt
        U(:,1) = U0;
        if ~isempty(outsideidxs)
            U(outsideidxs,1) = 0;
        end
        U(:,2) = U0-dt*dUdt0+0.5*(dt^2)/(dx^2)*A*U0+0.5*(dt^2)/(dx^2)*b;
        if ~isempty(outsideidxs)        
            U(outsideidxs,2) = 0;
        end

    end


    function [TT] = initialisationTT(U,Idx,Fracs)
    % [TT,IDX,FRACS] = INITIALISATIONTT(U,IDX,FRACS) This function 
    % initialisates the timetraces. Memory is allocated for the timetraces
    % and the first value is calculated. INITIALISATIONTT expects a
    % real vector U of dimension Nx*Ny a real value of real valued vector
    % IDX and a a real array of dimension FRACS as inputs. IDX must be an 
    % integer between 1 and Nx*Ny, Fracs must be a real value between 0 and
    % 1.
        
        if ~isreal(U)
            error('ERROR IN INITIALISATIONTT: U must be real')
        elseif size(U,1) ~= Nx*Ny
            error(['ERROR IN INITIALISATIONTT: size U ' ...
             'is incompatible with size grid'])
        end

        if ~isreal(Fracs)
            error('ERROR IN INITIALISATIONTT: Fracs must be real')
        elseif ~isempty(find(Fracs < 0,1)) || ~isempty(find(Fracs > 1,1))
            error(['ERROR IN INITIALISATIONTT: Fracs must be between 0 and ' ...
                '1'])
        end

        if ~isempty(find(mod(Idx,1) ~= 0,1))
            error('ERROR IN INITIALISATIONTT: Idx must be an integer')
        elseif ~isempty(find(Idx < 1,1)) ||  ~isempty(find(Idx > Nx*Ny,1))
            error(['ERROR IN INITIALISATIONTT: Idx must be a valid index' ...
                'of U'])
        end
  
        if length(Idx) ~= size(Fracs,1)
            error(['ERROR IN INITIALISATIONTT: Dimension Idx and Fracs ' ...
                'not compatible'])
        end
        % Initialisation memory for the timetraces
        TT = zeros(length(Idx),floor(T/dt)+1);

        % First value
        TT(:,1) = (1-Fracs(:,1) - Fracs(:,2)).*U(Idx,1) + Fracs(:,1).*U(Idx+1,1)...
                + Fracs(:,2).*U(Idx+Nx,1);

    end

    function [Idx, Fracs] = LocalisationTT
        % [Idx, Fracs] = LOCALISATIONTT This function determines the
        % location of the timetraces inside the grid. It will determine the
        % index/indices IDX of the vector U that correspond(s) to the mesh
        % point(s) left underneath the timetrace locations. It will also
        % determine Fracs that for every timetrace consists of two values
        % such that the location of the timetrace is


        MPx = (timetraces(:,1)-xmin(1))./dx; 
        MPy = (timetraces(:,2)-xmin(2))./dx;
        Idx = floor(MPx)+Nx*(floor(MPy)-1);
        Fracs = [MPx-floor(MPx),MPy-floor(MPy)];
    end


    function [U,TT] = timeStepping(U,A,B,C,b1,varargin)
    % [U,TT] = TIMESTEPPING(U,A,B,C,B1,F1,AX,AY,B2,TT, IDX, FRACS) This
    % function performs the time stepping by performing the following
    % expression for every discrete moment in time:
    % Unew = A*U(:,1) + B*U(:,2) + C*b. The timetraces are stored as well.
    % The function expects real arrays A, B and C, all of dimension
    % Nx*Ny-by-Nx*Ny. It expects a real U matrix of dimension Nx*Ny-by-2.
    % It expects a real vector b1 of length Nx*Ny. As optional  

        % Optional arguments
        arglist = cell(1,length(varargin)/2);
        for j = 1:length(varargin)/2
            arglist{j} = varargin{1+(j-1)*2};
        end
        arglist = cellstr(arglist);

        % initialisation b2 if specified
        if any(strcmp(arglist,'b2'))
            index2 = find(strcmp(arglist,'b2'));
            b2local =varargin{index2*2};
        end
    
        % initialisation of the timetraces and the values needed to calculate
        % these traces if timetraces are specified
        if any(strcmp(arglist,'TT'))
            index2 = find(strcmp(arglist,'TT'));
            cellTT = varargin{index2*2};
            TT = cellTT{1};
            Idxlocal = cellTT{2};
            Fracslocal = cellTT{3};
        end

        %
        t = 0;
        inst = 2;
        % Time stepping
        endT = T-dt;
        while t <= endT
            if tempdepbc == true
                b1 = CalculateB(f1,ax,ay,dx,xmin,xmax,t);
            end                
            if inclusion == true
                b=(dt^2)/(dx^2)*(b1+b2local);
            else
                b=(dt^2)/(dx^2)*b1;
            end
            
            Unew = A*U(:,1)+B*U(:,2) +C*b;                   

            % Update values
            U(:,2) = U(:,1); U(:,1) = Unew;
             
            % Draw evolution
            if evolution
                showEvolution(Unew);
            end
            
            % Update timetraces
            if  TTboolean
                TT(:,inst) = (1-Fracslocal(:,1) - Fracslocal(:,2)).*...
                U(Idxlocal,1) + Fracslocal(:,1).*U(Idxlocal+1,1)...
                    +Fracslocal(:,2).*U(Idxlocal+Nx,1);
                inst = inst + 1;
            end            
            
            t = t+dt;
            dt = min(dt,T-t);
        end
        
    end


    function showEvolution(U)
    % SHOWEVOLUTION(U) This function plots the wave intensity U. This way
    % the evolution of the intensity through time can be shown.
    % SHOWEVOLUTION expects a real valued vector of dimensions Nx*Ny as
    % input
         if length(U)~= Nx*Ny
            error(['ERROR IN SHOWEVOLUTION: size U ' ...
             'is incompatible with size grid'])
         elseif ~isreal(U)
             error(['ERROR IN SHOWEVOLUTION: U must be real valued ' ...
             'is incompatible with size grid'])
         end

        % U is reshaped into the approximate form.
        U = reshape(U,Nx,Ny);
        U = U';
        
        % The mesh is generated
        xcor = xmin(1)+dx:dx:xmax(1)-dx;
        ycor = xmin(2)+dx:dx:xmax(2)-dx;
        [X,Y] = meshgrid(xcor,ycor);

        % Plot is made and labeled
        surf(X,Y,U)
        zlim([-2,3]);
        xlim([0,1]);
        ylim([0,1]);
        xlabel('x')
        ylabel('y')
        zlabel('U')
        set(get(gca,'zLabel'),'Rotation',0)
        drawnow;
        
    end
end

function A = SpatialDiscretisationRectDomain(ax,ay,Nx,Ny)
% A = SPATIALDISCRETISATIONRECTDOMAIN(AX,AY,NX,NY) This function performs
% the spatial discretisation of the wave equation by approximating the
% spatial derivatives with a finite differences scheme. This scheme
% performs central differences twice sequentially to approximate the second
% order derivatives. This is done as follows for the x-derivative:
% delta^2 U(x,y)/(delta x)^2 = 
% (a(x+1/2dx,y)*U(x+dx,y) - (a(x+1/2dx,y)+a(x-1/2dx,y))U(x,y) + 
% a(x-1/2dx,y)*U(x-dx,y))/dx^2
% An approximation of this kind leads to a discretisation of the form:
% delta^2 U(x,y)/(delta x)^2 = 1/dx (A*U +b). This function determines the
% A-matrix in this scheme. SPATIALDISCRETISATIONRECTDOMAIN expects arrays 
% of real values AX and AY that contain the values of ax(x,y) and ay(x,y) 
% at an auxilary grid in between the main grid:
% AX = [ax(1/2*dx,dy),ax(3/2*dx,dy),...,A(1/2*dx,2*dy),...,A((Nx-1/2)*dx,(Ny-1)*dy)]
% AX = [ay(dx,1/2*dy),ax(dx,3/2*dy),...,ax(2*dx,1/2*dy),...,A((Nx-1)*dx,(Ny-1/2)*dy)]
% The function also expects integer inputs NX and NY that give the amount 
% of mesh points along repectively the x- and y-direction. It returns a
% real valued A-matrix.
    
    % Check whether ax and ay are arrays of real values
    if ~isreal(ax) || ~isreal(ay)
        error(['ERROR IN SPATIALDISCRETISATIONRECTDOMAIN: invalid ' ...
            'specification of the coefficients: must be real'])
    end
    
    % Check whether Nx and Ny are positive integers
    if mod(Nx,1) ~= 0  || mod(Ny,1) ~= 0  || Nx <=0 || Ny <= 0 
        error(['ERROR IN SPATIALDISCRETISATIONRECTDOMAIN: invalid ' ...
            'specification of the Nx and Ny: must be positive integers'])
    end
    
    
    % Check whether ax has the right size
    if length(ax)~= (Nx+1)*Ny
         error(['ERROR IN SPATIALDISCRETISATIONRECTDOMAIN: length ax ' ...
             'is incompatible with Nx'])
    end
    
     % Check whether ay has the right size
    if length(ay)~= Nx*(Ny+1)
         error(['ERROR IN SPATIALDISCRETISATIONRECTDOMAIN: length ay ' ...
             'is incompatible with Ny'])
    end
    

    % Discretisation of the x-derivative
    % Elements on the off and on diagonals for the x-derivative are created
    [axnew1,axnew2]  = removeaxatboundary;        
    % Initialisation
    i = [2:Nx*Ny,1:Nx*Ny,1:(Nx*Ny-1)];
    j = [1:(Nx*Ny-1),1:Nx*Ny,2:Nx*Ny];
    v = [axnew1',-axnew2',axnew1'];
            
    % Discretisation of the y-derivative: initialisation
    i(end+1:end+2*Nx*(Ny-1)+Nx*Ny) = [Nx+1:Nx*Ny,1:Nx*Ny,1:(Nx*Ny-Nx)];
    j(end+1:end+2*Nx*(Ny-1)+Nx*Ny) = [1:(Nx*Ny-Nx),1:Nx*Ny,Nx+1:Nx*Ny];
    v(end+1:end+2*Nx*(Ny-1)+Nx*Ny)= [ay(Nx+1:end-Nx)',...
        -ay(1:end-Nx)'-ay(Nx+1:end)',ay(Nx+1:end-Nx)'];
        
     % Creation of the sparse matrix
     A = sparse(i,j,v,Nx*Ny,Nx*Ny);


     function [axnew1,axnew2]  = removeaxatboundary
     % [AXNEW1,AXNEW2] = REMOVEAXATBOUNDARY(AX,NX) This function creates
     % two arrays that contain the off-diagonal and diagonal elements of 
     % the A-matrix that are needed to discretise the x-derivative. AXNEW1 
     % is created by removing all elements of AX that are next to
     % the outher boundary of the rectangular domain and inserting a zero. 
     % AXNEW2 is created by adding all neighbouring values of AX to each 
     % other.
     % REMOVEAXATBOUNDARY returns two arrays of real valued
     % values. AXNEW1 will contain the elements that will be on the
     % off-diagonals and AXNEW2 the elements that will be on the main
     % diagonal.

       
        % Creation of AXNEW1
        axnew1 = ax;
        % Removing all elements near boundary and inserting zeros 
        axnew1(1:Nx+1:end)=[];
        axnew1(Nx:Nx:end)=0;
        axnew1(end) =[];
          
        % Sum of ax with its shifted version
        axnew2 = ax(1:end-1)+ax(2:end);
        % Every time a new row is started, one element of this sum is 
        % skipped
        axnew2(end-Nx:-(Nx+1):1)= [];    
     end
end
    
function [A1,A2, b2,outsideidxs] = ...
    SpatialDiscretisationForInclusion(A,d,C,xmin,xmax,axfun,ayfun,f2,TypeIncl,f1)
    % A1 = SPATIALDISCRETISATIONFORINCLUSION(A,D,C,XMIN,
    % XMAX,AXFUN,AYFUN,F2,TYPEINCL,F1) This function applies the theory 
    % developped by Kreiss et al. A spatial discretisation of the wave 
    % equation obtained by finite differences is given. This discretisation
    % can be written in the following form: delta^2 U/(delta t)^2 = 
    % 1/dx^2(A*U +b). This function will adapt this discretisation so that 
    % the inclusion is taken into account. The resulting form will be: 
    % delta^2 U/(delta t)^2 = 1/dx^2((A1+A2)*U+b1+b2). In this form A1, A2,
    % b1 and b2 are formed. This is done according to the paper by Kreiss
    % et al. A2 will correspond to the matrix A where only the points
    % outside the inclusion are taken into account. A1 will take the
    % boundary points into account. B1 will correspond to the original
    % vector b that takes the outher boundary into account. B2 will take
    % the boundary of the inclusion into account.
    % The function SPATIALDISCRETISATIONFORINCLUSION expects a real valued
    % matrix A of a size that corresponds to the size of the grid. It
    % expects a real valued discretisation D that is either of dimension 1
    % (dx = dy = D) or dimension 2 (dx = D(1), dy = D(2)). It expects a
    % function handle C that has two input arguments. The same holds for
    % axfun and ayfun. The variables XMIN and XMAX must be real valued
    % arrays of dimension two that make physically sense (XMAX>XMIN). F2
    % needs to be function handle with two input arguments. The same holds
    % for TypeIncl. F1 is an optional argument, but needs to be a function 
    % handle with 2 or 3 input arguments if specified. The function returns
    % a real valued sparse matrix A1.
    %
    % [A1,A2] = SPATIALDISCRETISATIONFORINCLUSION(A,D,C,XMIN,
    % XMAX,AXFUN,AYFUN,F2,TYPEINCL,F1) returns the sparse matrix A2 as
    % well. Once again this function is defined according to Weiss et al.
    %
    % [A1,A2,B2] = SPATIALDISCRETISATIONFORINCLUSION(A,D,C,XMIN,
    % XMAX,AXFUN,AYFUN,F2,TYPEINCL,F1) returns a sparse vector for B2 as
    % well.
    %
    % [A1,A2,B2,OUTSIDEIDXS] = SPATIALDISCRETISATIONFORINCLUSION(A,D,C,
    % XMIN,XMAX,AXFUN,AYFUN,F2,TYPEINCL,F1) OUTSIDEIDXS will be a value of
    % integers with the indices of U that are inside the inclusion.

    % Check whether the rectangular domain is correctly specified and that
    % it is two-dimensional
    if ~isreal(xmin) || ~isreal(xmax) || length(xmin)~=2 || length(xmax)~=2
        error('ERROR In adaptAforInclusion: Invalid domain entered')
    end

    % Check whether the discretisation is specified correctly: real values
    % and maximum 2 components
    if ~isreal(d) || length(d) > 2
        error(['ERROR In adaptAforInclusion: Invalid discretisation ' ...
                'entered'])
    % If it is specified correctly, dx and dy can be specified
    elseif length(d) == 1
        dx = d; dy = d;
    else
        dx = d(1); dy = d(2);
    end

    % Number of meshpoints are calculated
    Nx = (xmax(1) - xmin(1))/dx - 1;
    Ny = (xmax(2) - xmin(2))/dy - 1;    

    % Check that A is specified correctly and that it has the right
    % dimensions
    if ~isreal(A)
        error('ERROR In adaptAforInclusion: Invalid A-matrix entered')
    elseif  size(A,1) ~= Nx*Ny || size(A,2) ~= Nx*Ny
        error('ERROR In adaptAforInclusion: dimensions A-matrix incorrect')        
    end
    
    % Check that C is specified correctly and that it has the right amount
    % of arguments
    if ~isa(C,'function_handle') 
        error(['ERROR In adaptAforInclusion: Contour curve needed to ' ...
                'for the inclusion'])
    elseif nargin(C) ~= 2
        error(['ERROR In adaptAforInclusion: Invalid  curve defined for the ' ...
            'inclusion: must have exactly two variables'])
    end

    % Check that ax and ay are specified correctly and that they have 
    % the right amount of arguments
    if ~isa(axfun,'function_handle') || ~isa(ayfun,'function_handle')
        error('ERROR In adaptAforInclusion: coefficient function needed')
    elseif nargin(axfun)~=2 || nargin(ayfun)~=2
        error('ERROR In adaptAforInclusion: invalid coefficient function')
    end

    % Check that the function f1 and TypeIncl are specified correctly and
    % that they have the right amount of arguments
    if ~isa(f2,'function_handle') || ~isa(TypeIncl,'function_handle')
        error(['ERROR In adaptAforInclusion: invalid specification for ' ...
                'inclusion: f2 and/or TypeIncl are no function handles'])
    elseif nargin(f2)  ~= 2
        error(['ERROR In adaptAforInclusion: invalid f2-function: must have ' ...
            '2 variables'])
    elseif ~isa(TypeIncl,'function_handle')
        error(['ERROR In adaptAforInclusion: invalid TypeIncl-function: must ' ...
            'have 2 variables'])        
    end

    % Check that if f1 is a specified, it is specified as a function handle 
    if  ~isempty(f1)
        if ~isa(f1,'function_handle')
            error('ERROR In adaptAforInclusion: invalid specification for f1')
        elseif nargin(f1) ~= 2 && nargin(f1)~= 3
            error('ERROR In adaptAforInclusion: invalid number of arguments for f1')
        end
    
    end

    
    % Allocation memory matrix A1 and vector b
    b2 = zeros(Nx*Ny,1);
    A1 = sparse(Nx*Ny,Nx*Ny);

    % Initialisation of A2
    A2 = A;

    % Label all mesh points
    [M,BCNeighbours] = determineLocationMesh;
    
    % Adapt cells of the A2-matrix that correspond to meshpoints inside the
    % inclusion and hence outside the domain
    outsideidxs = find(M == 0);
    A2 = DisregardMeshpointsInsideInclusion(A2,outsideidxs);
    
    % Adapt cells of the A1-matrix that correspond to boundary points
    bcidxs = find(M == -1);
    [A1,A2,b2] = AdaptAandbforBoundaryPoints(A2,A1,b2,bcidxs,BCNeighbours);
   

    
    function [M,NB] = determineLocationMesh
    % [M,NB] = DETERMINELOCATIONMESH(XMIN,XMAX,C,D) labels all mesh points
    % of the rectangular domain as point outside the inclusion (m = 1),
    % inside the inclusion (m = 0) or boundary point (a point that has a 
    % neigbouring mesh point inside the inclusion) (m = -1). The labels
    % of all mesh points are collected in a vector M that goes over the
    % mesh points row by row. For the boundary points, the function
    % determines on which side the boundary is located. Four situations can
    % occur:
    %  - Situation 1: inclusion is on the right of the boundary point,
    %       first column of NB gets a value of 1
    %  - Situation 2: inclusion is above the boundary point,
    %       first column of NB gets a value of 2
    %  - Situation 3: inclusion is on the left of the boundary point,
    %       first column of NB gets a value of 3
    %  - Situation 4: inclusion is underneath of the boundary point,
    %       first column of NB gets a value of 4
    % DETERMINELOCATIONMESH returns a  vector of values 1, 0  and -1 for M 
    % and a matrix of 0's and 1's for NB

        % Determine nb of mesh points in the two directions
        Nx = (xmax(1)-xmin(1))/dx-1;
        Ny = (xmax(2)-xmin(2))/dy-1;
    
        % allocation memory for the output variables
        M = zeros(Ny*Nx,1);
        NB = sparse(Ny*Nx,4);
        
        % Initiation of the vectors that contain the mesh points
        ind = 1:Ny*Nx;
        X1 = mod(ind,Nx);
        X1(X1==0)=Nx;
        Y1 = (floor(ind/Nx)+1);

        % Find all points outside the inclusion and label them
        CC = C(xmin(1)+X1*dx,xmin(2)+Y1*dy); % evaluate contour function for all mesh points
        w = find(CC>5*10^(-15)); % Select the ones with a positive value
        M(w)=1; % Label accordingly

        % Determine for all points outside the inclusion which are boundary
        % points 
        % Check the right side of the mesh point
        CC1 = C(xmin(1)+X1(w)*dx+dx,xmin(2)+Y1(w)*dy); % Evaluate all right neigbours
        w1 = find(CC1<=5*10^(-15)); % Select the ones that are inside the inclusion
        NB(w(w1),1)=1; % Give them a value of 1 inside NB

        % Check above he mesh point
        CC2 = C(xmin(1)+X1(w)*dx,xmin(2)+Y1(w)*dy+dy); % Evaluate all neighbours above
        w2 = find(CC2<=5*10^(-15)); % Select the ones that are inside the inclusion
        NB(w(w2),2)=1; % Give them a value of 1 inside NB

        % Check the left side of the mesh point
        CC3 = C(xmin(1)+X1(w)*dx-dx,xmin(2)+Y1(w)*dy); % Evaluate all left neigbours
        w3 = find(CC3<=5*10^(-15)); % Select the ones that are inside the inclusion
        NB(w(w3),3)=1; % Give them a value of 1 inside NB

        % Check under the mesh point
        CC4 = C(xmin(1)+X1(w)*dx,xmin(2)+Y1(w)*dy-dy);  % Evaluate all neigbours underneath
        w4 = find(CC4<=5*10^(-15)); % Select the ones that are inside the inclusion
        NB(w(w4),4)=1; % Give them a value of 1 inside NB
        
        % Collect all points that have a neighbour inside the inclusion and
        % label them boundary points
        ws = find(CC1<=5*10^(-15) | CC2<=5*10^(-15) | CC3<=5*10^(-15) | CC4<=5*10^(-15));
        M(w(ws))=-1;


    end

    function A2 = DisregardMeshpointsInsideInclusion(A2,outsideidxs)
        % A2 = DISREGARDMESHPOINTSINSIDEINCLUSION(A2,OUTSIDEIDXS) This
        % function puts all values of A2 in the rows and columns with
        % indices equal to OUTSIDEIDXS to zero. As a result, the value of 
        % V = A2*U at the indices OUTSIDEIDXS is equal to 0. The value of V
        % at the other indices are not influenced by the value of U at 
        % indices OUTSIDEIDXS.
        % DISREGARDMESHPOINTSINSIDEINCLUSION returns an adapted matrix A2
        % of real values.
     
        % Rows are set to 0
        A2(outsideidxs,:) = 0;

        % Columns are set to 0
        A2(:,outsideidxs) = 0;

    end


    function [A1,A2,b] = AdaptAandbforBoundaryPoints(A2,A1,b,bcidxs,BCNeighbours)
    % A1 = ADAPTAANDBFORBOUNDARYPOINTS(A2,A1,B,BCIDXS,BCNEIGHBOURS)
    % adapts the matrix A1 in case of Dirichlet boundary conditions on the
    % inclusion according to the theory developped by Weiss et al. It takes
    % the influence of the boundary points on the discretisation into
    % account. Also the vector B is created. This vector takes the values
    % on the boundary of the inclusion into account. An adapted sparse
    % matrix A1 is returned.
    %
    % [A1,A2] = ADAPTAANDBFORBOUNDARYPOINTS(A2,A1,B,BCIDXS,BCNEIGHBOURS)
    % also returns the matrix A2 as sparse matrix.
    % 
    % [A1,A2,B] = ADAPTAANDBFORBOUNDARYPOINTS(A2,A1,B,BCIDXS,BCNEIGHBOURS)
    % also returns the vector B as sparse vector.
        
        for idx = 1:length(bcidxs)
            i = bcidxs(idx);
            x = xmin(1)+mod(i,Nx)*dx;
            if x == 0
                x = xmin(1)+Nx*dx;
            end
            y = xmin(2)+(floor(i/Nx)+1)*dy;
            
            nb = find(BCNeighbours(i,:)==1);

            for k = nb
                alpha = determineAlphaforNode(x,y,k);    
                if k == 1
                    typebc = TypeIncl(x+alpha*dx,y);
                    b(i) = b(i) + axfun(x+0.5*dx,y)*f2(x+alpha*dx,y)/alpha;
                    A1(i,i) = A1(i,i)-axfun(x+0.5*dx,y)*(1-alpha)/alpha;      
                elseif k == 2             
                    typebc = TypeIncl(x,y+alpha*dy);
                    b(i) = b(i) + ayfun(x,y+0.5*dy)*f2(x,y+alpha*dy)/alpha;
                    A1(i,i) = A1(i,i)-ayfun(x,y+0.5*dy)*(1-alpha)/alpha;
    
                elseif k == 3                 
                    typebc = TypeIncl(x-alpha*dx,y);
                    b(i) = b(i) + axfun(x-0.5*dx,y)*f2(x-alpha*dx,y)/alpha;
                    A1(i,i) = A1(i,i)-axfun(x-0.5*dx,y)*(1-alpha)/alpha;

                elseif k == 4                  
                    typebc = TypeIncl(x,y-alpha*dy);
                    b(i) = b(i) + ayfun(x,y-0.5*dy)*f2(x,y-alpha*dy)/alpha;
                    A1(i,i) = A1(i,i)- ayfun(x,y-0.5*dy)*(1-alpha)/alpha;
                end
            end
         end
    end


    function [alpha,exitflag] = determineAlphaforNode(x,y,direction)
    %  ALPHA = DETERMINEALPHAFORNODE(X,Y,DIRECTION) This function
    %  looks over the interval between the meshpoint (X,Y) and its
    %  neigbouring meshpoint in the direction according to DIRECTION for
    %  the point where C(X,Y) = 0. Four situations can occur:
    %   - SITUATION 1: Interval to the right of meshpoint (X,Y)
    %      -> DIRECTION =  1 
    %      -> interval = [X,X+DX]x[Y], ALPHA is searched such that
    %         C(X+ALPHA*DX,Y) = 0 
    %         if C(zerop,Y) = 0, then ALPHA = (zerop - X)/DX
    %   - SITUATION 2: Interval above the meshpoint (X,Y)
    %      -> DIRECTION =  2
    %      -> interval = [X]x[Y,Y+DY], ALPHA is searched such that
    %         C(X,Y+ALPHA*DY) = 0 
    %         if C(X,zerop) = 0, then ALPHA = (zerop - Y)/DY
    %   - SITUATION 3: Interval to the left of the meshpoint (X,Y)
    %      -> DIRECTION =  3
    %      -> interval = [X-DX,X]x[Y], ALPHA is searched such that
    %         C(X-ALPHA*DX,Y) = 0 
    %         if C(zerop,Y) = 0, then ALPHA = -(zerop - X)/DX
    %   - SITUATION 4: Interval underneath the meshpoint (X,Y)
    %      -> DIRECTION =  4
    %      -> interval = [X]x[Y,Y-DY], ALPHA is searched such that
    %         C(X,Y+ALPHA*DY) = 0 
    %         if C(X,zerop) = 0, then ALPHA = (zerop - Y)/DY
    % It searches the value zerop by using the fzero function over the
    % corresponding interval. DETERMINEALPHAFORNODE accepts real valued
    % inputs X and Y and returns a real valued output ALPHA.
    % DETERMINEALPHAFORNODE also expects an input DIRECTION that has an 
    % integer value equal to 1, 2, 3 or 4.
    %
    % [ALPHA,EXITFLAG] = DETERMINEALPHAFORNODE(X,Y, DIRECTION) returns
    % an EXITFLAG that describes the exit condition: 1 = succesful
    % determination of alpha, -1 = unsuccesful determination of alpha


        if ~isreal(x) ||  ~isreal(y)
            error('ERROR In determineAlphaforNode: Invalid mesh point entered')
        end
        if direction ~= 1 && direction ~= 2 && direction ~= 3 && direction~= 4
            error('ERROR In determineAlphaforNode: Invalid direction')
        end

        % Search interval [x,x+dx]x[y]  for point where C = 0
        if direction == 1 
            f = @(k) (C(k,y)); % Redefine so that only x is a variable
            if sign(f(x)) == sign(f(x+dx))
                alpha =1;
                exitflag = 1;
            else
                [zerop,~,exitflag] = fzero(f,[x x+dx]); % Search interval
                alpha = abs(x-zerop)/dx; % Calculate alpha
            end
        % Search interval [x]x[y+dx]  for point where C = 0
        elseif direction == 2 
            f = @(k) (C(x,k)); % Redefine so that only y is a variable       
            if sign(f(y)) == sign(f(y+dy))
                alpha =1;
                exitflag = 1;
            else
                [zerop,~,exitflag] = fzero(f,[y y+dy]); % Search interval
                alpha = abs(y-zerop)/dy; % Calculate alpha
            end
        % Search interval [x,x-dx]x[y]  for point where C = 0            
        elseif direction == 3
            f = @(k) (C(k,y)); % Redefine so that only x is a variable
            if sign(f(x)) == sign(f(x-dx))
                alpha =1;
                exitflag = 1;
            else
                [zerop,~,exitflag] = fzero(f,[x-dx x]);  % Search interval
                alpha = abs(x-zerop)/dx; % Calculate alpha
            end
        % Search interval [x]x[y,y-dx]  for point where C = 0            
        else 
            f = @(k) (C(x,k)); % Redefine so that only y is a variable
            if sign(f(y)) == sign(f(y-dy))
                alpha =1;
                exitflag = 1;                
            else
                [zerop,~,exitflag] = fzero(f,[y-dy y]);  % Search interval
                alpha = abs(y-zerop)/dy; % Calculate alpha
            end
        end

        % Put flag to -1 if fzero did not find a zero in the interval
        if exitflag < 0
            exitflag = -1;
        end
    end
end



function b = CalculateB(f1,ax,ay,d,xmin,xmax,t,Unew)
% B = CALCULATEB(F1,AX,AY,D,XMIN,XMAX,T) This function performs
% the spatial discretisation of the wave equation by approximating the
% spatial derivatives with a finite differences scheme. This scheme
% performs central differences twice sequentially to approximate the second
% order derivatives. This is done as follows for the x-derivative:
% delta^2 U(x,y)/(delta x)^2 = 
% (a(x+1/2dx,y)*U(x+dx,y) - (a(x+1/2dx,y)+a(x-1/2dx,y))U(x,y) + 
% a(x-1/2dx,y)*U(x-dx,y))/dx^2
% An approximation of this kind leads to a discretisation of the form:
% delta^2 U(x,y)/(delta x)^2 = 1/dx (A*U +b). This function calculates the
% vector b in this scheme. This vector takes the boundary into account
% For every meshpoint that is next to a boundary, the differences
% approximation needs the value of U at a point on the boundary. Since 
% there are no mesh points defined on the boundary, the vector b needs to
% solve this. The value of U at the boundary is known through the boundary
% conditions. Four situations can occur:
% - Situation 1: mesh point next to the left boundary (x = xmax)
%    -> a(x-1/2dx,y)*U(x-dx,y) = a(x-1/2dx,y)*f1(x-dx,y)
%    -> occurs for the first meshpoint of every row
% - Situation 2: mesh point next to the right boundary (x = xmin)
%    -> a(x-1/2dx,y)*U(x+dx,y) = a(x-1/2dx,y)*f1(x+dx,y)
%    -> occurs for the last meshpoint of every row
% - Situation 3: mesh point next to the bottom boundary (y = ymin)
%    -> a(x,y-1/2dy)*U(x,y-dy) = a(x,y-1/2dy)*f1(x,y-dy) 
%    -> occurs for the meshpoints in the first row
% - Situation 4: mesh point next to the upper boundary (y = ymax)
%    -> a(x,y+1/2dy)*U(x,y+dy) = a(x,y+1/2dy)*f1(x,y+dy)
%    -> occurs for the last meshpoint of last row
% The value of b can be both time dependent or independent.
% The function CALCULATEB expects a function handle F1 as input that has
% x, y and possibly t as arguments. It expects arrays of real values AX and
% AY that contain the values of ax(x,y) and ay(x,y) at an auxilary grid in 
% between the main grid:
% AX = [ax(1/2*dx,dy),ax(3/2*dx,dy),...,A(1/2*dx,2*dy),...,A((Nx-1/2)*dx,(Ny-1)*dy)]
% AX = [ay(dx,1/2*dy),ax(dx,3/2*dy),...,ax(2*dx,1/2*dy),...,A((Nx-1)*dx,(Ny-1/2)*dy)]
% The function expects a real valued D of dimension 1 (dx = dy = D)
% or 2 (dx = D(1), dy = D(2)). XMIN and XMAX must be real valued arrays of
% dimension 2. T is an optional argument that must be specified if f1 is
% time dependent. In that case it must be real valued. CALCULATEB returns
% a real valued array B.
    
    % Check whether the discretisation is specified correctly: real values
    % and maximum 2 components
    if ~isreal(d) || length(d) > 2
        error(['ERROR In CalculateB: Invalid discretisation ' ...
                'entered'])
    % If it is specified correctly, dx and dy can be specified
    elseif length(d) == 1
        dx = d; dy = d;
    else
        dx = d(1); dy = d(2);
    end

    % Number of nodes along x- and y-direction
    Nx = (xmax(1)-xmin(1))/dx-1;
    Ny = (xmax(2)-xmin(2))/dy-1;

    % Check whether Nx and Ny are positive integers
    if mod(Nx,1) ~= 0  || mod(Ny,1) ~= 0  || Nx <=0 || Ny <= 0 
        error(['ERROR IN CalculateB: invalid ' ...
            'specification of the domain: xmin > xmax'])
    end

    % Check that ax and ay are specified correctly and that they have 
    % the right amount of arguments
    if ~isa(f1,'function_handle')
        error(['ERROR In CalculateB: Invalid f1 entered: function handle' ...
            'required'])
    elseif nargin(f1) ~= 2 && nargin(f1) ~= 3
        error(['ERROR In CalculateB: Invalid f1 entered: function handle' ...
            'with inappropriate amount if arguments'])
    end

    % Check whether ax and ay are arrays of real values
    if ~isreal(ax) || ~isreal(ay)
        error(['ERROR IN CalculateB: invalid ' ...
            'specification of the coefficients: must be real'])
    end

    % Check whether ax has the right size
    if length(ax)~= (Nx+1)*Ny
         error(['ERROR IN CalculateB: length ax ' ...
             'is incompatible with Nx'])
    end

    % Check whether ay has the right size    
    if length(ay)~= Nx*(Ny+1)
         error(['ERROR IN CalculateB: length ay ' ...
             'is incompatible with Ny'])
    end

    % Check whether the rectangular domain is correctly specified and that
    % it is two-dimensional
    if ~isreal(xmin) || ~isreal(xmax) || length(xmin)~=2 || length(xmax)~=2
        error('ERROR In CalculateB: Invalid domain entered')
    end
    
    % Check whether t is specified if f1 is time dependent and if it is
    % real valued in that case
    if nargin(f1) == 3 
        if isempty(t)
            error('ERROR In CalculateB: value of t must be specified')
        elseif ~isempty(t) && ~isreal(t)
            error('Error In CalculateB: value of t must be real')
        end
    end
     
    
    % Allocation memory for the output
    b = zeros(Nx*Ny,1);

    % Creation of arrays that contain the indices of b where the
    % boundary needs to be taken into account:
    % - boundary x = xmin(1): left boundary
    w1 = 1:Nx:Nx*Ny;
    % - boundary x = xmax(1): right boundary
    w2 = Nx:Nx:Nx*Ny;
    % - boundary y = xmin(2): boundary below
    w3 = 1:Nx;
    % - boundary y = xmax(2): boundary above
    w4 = Nx*(Ny-1)+1:Nx*Ny;

    % Creation of arrays that contain the indices of ax next to the 
    % boundary 
    % - boundary x = xmin(1): left boundary
    ws1 = 1:Nx+1:(Nx+1)*Ny;
    % - boundary x = xmax(1): right boundary
    ws2 = Nx+1:Nx+1:(Nx+1)*Ny;

    % Creation of arrays that contain the indices of ay next to the 
    %boundary
    % - boundary y = xmin(2): boundary below
    ws3 = w3;
    % - boundary y = xmax(2): boundary above
    ws4 = Nx*Ny+1:(Ny+1)*Nx;

    % Auxilary vectors
    l1 = ones(1,Nx); l2 = ones(1,Ny);
    k = 1:Nx;
    m = 1:Ny;
        
    % B is determined for two situations:
    % Situation where f1 is independent of time
    if nargin(f1) == 2
        % Left boundary is taken into account
        b(w1) = ax(ws1).*f1(xmin(1).*l2',xmin(2)+m'*dy);
        % Right boundary is taken into account
        b(w2) = ax(ws2).*f1(xmax(1).*l2',xmin(2)+m'*dy);

        % Bottom boundary is taken into account
        b(w3) = b(w3)+ay(ws3).*f1(xmin(1)+k'*dx,xmin(2).*l1');
        % Upper boundary is taken into account
        b(w4) = b(w4)+ay(ws4).*f1(xmin(1)+k'*dx,xmax(2).*l1');

    % Situation where b is dependent on time
    else
        % Left boundary is taken into account        
        b(w1) = ax(ws1).*f1(xmin(1).*l2',xmin(2)+m'*dy,t);
        % Right boundary is taken into account        
        b(w2) = ax(ws2).*f1(xmax(1).*l2',xmin(2)+m'*dy,t); 
       
        % Bottom boundary is taken into account        
        b(w3) = b(w3)+ay(ws3).*f1(xmin(1)+k'*dx,xmin(2).*l1',t);                   
        % Upper boundary is taken into account        
        b(w4) = b(w4)+ay(ws4).*f1(xmin(1)+k'*dx,xmax(2).*l1',t);

    end
end