function Result = SolveFWProblem(WavEqSpecs,DataAssimSpecs,dx,param)
% RESULT = SOLVEFWPROBLEM(WAVEQSPECS,DATAASSIMSPECS,DX,PARAM) easier for 
% solver of the forward problem. It only requires objects that specify the
% wave equation and data assimilation contexts (WAVEQSPECS and 
% DATAASSIMSPECS), the precision DX with which the simulation is done and 
% the values of the parameters PARAM that characterise the inclusions. 
% The function converts these objects to the right specifications for the 
% Simulate function and applies this function to receive time traces. These
% are returned as output

    %% Problem Specifications
    % Timespan
    T = WavEqSpecs.T;

    % rectangular domain
    xmin = WavEqSpecs.xmin;
    xmax = WavEqSpecs.xmax;

    % Coefficients
    ax = WavEqSpecs.ax;
    ay = WavEqSpecs.ay;

    %% Specifications data assimilation
    % location timetraces
    timetraces = DataAssimSpecs.timetraces;
    
    % Location and angle wave
    origin = DataAssimSpecs.originWave;
    angle = DataAssimSpecs.Angle; % in radians
    
    % Initial conditions wave
    Tmatrix = [cos(angle), sin(angle); -sin(angle),cos(angle)]; 
    g = @(x,y) (exp(-250.*((Tmatrix(1,1).*(x-origin(1))+Tmatrix(1,2).*(y-origin(2))).^2)));
    h = @(x,y) (exp(-250.*((Tmatrix(1,1).*(x-origin(1))+Tmatrix(1,2).*(y-origin(2))).^2))...
    .*sqrt(0.5*(ax(x,y)+ay(x,y)))*500.*(Tmatrix(1,1).*(x-origin(1))+Tmatrix(1,2).*(y-origin(2))));
    
    f1 = @(x,y,t) (exp(-250.*((Tmatrix(1,1).*(x-origin(1))+Tmatrix(1,2).*(y-origin(2))-t).^2)));
    

    
    %% Specifications Inclusion
    C = @(x,y) WavEqSpecs.Cfun(x,y,param);

    % U on the inclusion
    f2 = WavEqSpecs.f2;
    TypeIncl = WavEqSpecs.TypeIncl;

    [~,Result] = Simulate(dx, T, xmin, xmax, ax,ay,g, h, f1,...
        "inclusion",{C,f2,TypeIncl},'timetraces',timetraces);
    
    
end