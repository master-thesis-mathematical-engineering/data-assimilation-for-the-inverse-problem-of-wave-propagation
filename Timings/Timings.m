clear all; close all; clc;
%% Script
% Goal: Determine Timings
% complexity: T/dt*((L/dx)^2)^2= L^4*T/(dx^5)
% PointSource
% Inclusion: Ellips, Dirichlet BC
% No variable coefficients
% Homogeneous BC


%% Specifications problem
% Time- and space-interval
T = 0.5;
xmin = [0,0];
xmax = [1,1];


% Coëfficiëntss
ax = @(x,y) (1+0.*x+0.*y);
ay = @(x,y) (1+0.*x+0.*y);

% Timetraces (None)
timetraces = [];

% Specify properties planar wave
origin = [0.1, 0];
angle = pi/4; % in radians
Tmatrix = [cos(angle), -sin(angle); sin(angle),cos(angle)]; % rotation in clockwise direction

% Offset
Offset = 0; 

% InitialConditions
g = @(x,y) (Offset + exp(-250.*((Tmatrix(1,1).*(x-origin(1))+Tmatrix(1,2).*(y-origin(2))).^2)));
h = @(x,y) (exp(-250.*((Tmatrix(1,1).*(x-origin(1))+Tmatrix(1,2).*(y-origin(2))).^2))...
    .*sqrt(0.5*(ax(x,y)+ay(x,y)))*500.*(Tmatrix(1,1).*(x-origin(1))+Tmatrix(1,2).*(y-origin(2))));


% Boundary Conditions
f1 = @(x,y,t) (Offset+exp(-250.*((Tmatrix(1,1).*(x-origin(1))...
    +Tmatrix(1,2).*(y-origin(2))-sqrt(0.5*(ax(x,y)+ay(x,y)))*t).^2)));

% Inclusion
C = @(x,y) (((x-0.5)/0.2).^2+((y-0.5)/0.1).^2-1);

% U on the inclusion
f2 = @(x,y) (0.*x+0.*y);
TypeIncl = @(x,y) ("Dirichlet");



%% Discretisation
% subsequent refinements
list = [0.03125/2,0.03125/4,0.03125/8,0.03125/16,0.03125/32,0.03125/64];
%list = [0.03125/64];

%% Allocation memory
TimingsArray = zeros(1,length(list));
TimingsArray2 = zeros(1,length(list));
TimingsArray3 = zeros(1,length(list));
TimingsArray4 = zeros(1,length(list));
TimingsArray5 = zeros(1,length(list));
TimingsArray6 = zeros(1,length(list));


%% Execution of the different simulations
%for j = 1:3
    for i = 1:length(list)
        % Discretisation        
        dx = list(i);
        dt = dx*0.5;
          
        
        % Apply finite differences
        fullSimul = tic;
        [U,~,Tmatrixmul,Tinversion,TConstrMatrices,TBCupdate,Tdiscr] = Simulate(dx, T, xmin, xmax, ax,ay,g, h, f1, ...
            "dt",dt,"inclusion",{C,f2,TypeIncl});

        TimingsArray(i) = toc(fullSimul);
        TimingsArray2(i) = Tmatrixmul;
        TimingsArray3(i) = Tinversion; 
        TimingsArray4(i) = TConstrMatrices;
        TimingsArray5(i) = TBCupdate;
        TimingsArray6(i) = Tdiscr;

    end
%end

figure(1)
loglog(list,[TimingsArray;TimingsArray2; ...
    TimingsArray3;TimingsArray4;TimingsArray5;TimingsArray6])
xlabel('dx')
ylabel('Timings')
legend('Full Simulation','Matrix Multiplications',...
    'Inversion matrix', 'Construction Am, Bm, Cm','UpdateBc','Discretisation')

figure(2)
loglog(list,[TimingsArray(1)./(1000*list.^3);TimingsArray])
xlabel('dx')
ylabel('Timings')
legend('Reference line','Full Simulation')