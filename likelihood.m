function p = likelihood(TTold,TTnew,TT,Tstart,noiselevel)
% P = LIKELIHOOD(TTOLD,TTNEW,TT,TTSTART,NOISELEVEL) This function
% calculates the ratio of the likelihood of two parameter values. It
% expects the deterministic time traces TTOLD and TTNEW that these 
% parameters generate and the amount of noise NOISELEVEL that is introduced 
% and the measured time trace TT. Also a start values Tstartstarting from 
% which the time traces should be used. Both likelihoods are gaussian.
% Their proportion is calculated with log likelihoods that are converted to
% the actual a value before returning this output. 

    % Specification of the covariance matrix based on the amount of noise
    SigmaInv = 1/(noiselevel^2)*eye(length(TT));

    % Calculation of the log likelihoods
    exponent = 0;
    for i = 1:size(TT,1)
        start = Tstart(i);
        exponent = exponent-0.5*(TT(i,start:end)-TTnew(i,start:end))*...
            SigmaInv(start:end,start:end)*(TT(i,start:end)-TTnew(i,start:end))'...
            +0.5*(TT(i,start:end)-TTold(i,start:end))*SigmaInv(start:end,start:end)*...
            (TT(i,start:end)-TTold(i,start:end))';
    end

    % Convert log likelihood to the actual a value
    p=exp(exponent);
end