function a = likelihoodAproxBayes(TTold,TTnew,TT,dt,sigma1,sigma2)
% A = LIKELIHOODAPPROXBAYES(TTOLD,TTNEW,TT,DT,SIGMA1,SIGMA2) This function
% calculates the ratio of the likelihood of two parameter values. These 
% likelihoods are defined according to the ABC approximation. The function
% expects the deterministic time traces TTOLD and TTNEW that these
% parameters generate. Also the measured time trace TT is
% required. Moreover, the value of the time steps DT and the variances
% SIGMA1 and SIGMA2 are required as inputs. Both likelihoods are gaussian. 
% Their proportion is calculated with log likelihoods that are converted to
% the actual a value before returning this output. 
    
    exponent = 0;
    for i = 1:size(TT,1)

        % Find the values where the time traces reach their minimum and the
        % values of these minima
        local_minima_old = islocalmin(TTold(i,:));
        minima_old = TTold(i,local_minima_old);
        minTT_old = minima_old(find(minima_old < -0.01,1));
        idxminTT_old = find(TTold(i,:) == minTT_old,1);
        TtominTT_old = (idxminTT_old-1)*dt;
        
        
        minTT = min(TT(i,:));
        idxminTT = find(TT(i,:) == minTT,1);       
        TtominTT = (idxminTT-1)*dt;

        local_minima_new = islocalmin(TTnew(i,:));
        minima_new = TTnew(i,local_minima_new);
        minTT_new = minima_new(find(minima_new < -0.01,1));        
        idxminTT_new = find(TTnew(i,:) == minTT_new,1);
        TtominTT_new = (idxminTT_new-1)*dt;

        % Caluclate the log likelihoods of these statistics
        exponent = exponent - 0.5*(minTT-minTT_new)^2/sigma1...
            - 0.5*(TtominTT-TtominTT_new)^2/sigma2...
            + 0.5*(minTT-minTT_old)^2/sigma1...
            +0.5*(TtominTT-TtominTT_old)^2/sigma2;
    end

    % Convert these values to the actual a value
    a = exp(exponent);

end
