function [p,mean] = prior(param,xmin,xmax)
% [P,MEAN] = PRIOR(PARAM,XMIN,XMAX) expression for the prior density of the
% parameters. It expects vectors that specify the lower limits and upper
% limitsfor the parameters (XMIN and XMAX) and the value of the parameters
% PARAM for which the density is evaluated.

    
    % Check whether parameter is inside the coverage of the density
    if param >= xmin
        if param <= xmax

            % Area/volume over which the distribution spreads out is
            % calculated
            A=1;
            for i = 1:length(param)
                A = A*(xmax(i)-xmin(i));
            end
        p = 1/A;
        else
            p=0;
        end
    else
        p = 0;
    end
    mean = xmin+(xmax-xmin)/2;
end