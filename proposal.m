function w = proposal(u,nbOfInclusions,beta)
% W = PROPOSAL(U, NBOFINCLUSIONS,BETA,C) This function generates a trial
% move based on a Gaussian transitional density. It requires the value of
% the previous sample U, the number of inclusions NBOFINCLUSIONS and 
% the parameters beta tp fully characterise the proposal density. The
% function proposes a new sample W and returs this as output. The following
% relation holds: w = u +beta*C*n with n a standard normal variable

    % Specify the covariance matrix of the normal distribution
    C =eye(length(u));
    C(3:3:nbOfInclusions*3,3:3:nbOfInclusions*3) = 1/6;     

    % Generate the trial move
    i = C*randn(size(u,1),size(u,2))';
    w = u + beta*i';
end